# Expérience contrôlée et analyse des résultats :

En premier lieu, il faut ajouter en en-tête du fichier response.csv :

`numero utilisateur, repetition, numero sequence, densite, taille, technique, temps, erreur`

Ensuite, dans R, il faut faire ceci pour importer le csv :

```
library(readr)
data <- read_csv("response.csv")
```

Si `readr` n'est pas installé, il faut faire :

```
install.packages("tidyverse")
```

Pour pouvoir voir la data il faut faire :

```
View(data)
```

Avant d'utiliser la data, il faut marquer les colonnes adéquates comme des facteurs et supprimer les lignes ou l'utilisateur a fait des erreurs.

La commande pour marquer les colonnes adéquates comme des facteurs :

```
data_clean$numero_utilisateur <- as.factor(data_clean$numero_utilisateur)
data_clean$densite <- as.factor(data_clean$densite)
data_clean$taille <- as.factor(data_clean$taille)
data_clean$technique <- as.factor(data_clean$technique)
data_clean$repetition <- as.factor(data_clean$repetition)
```

La commande pour retirer les erreurs des utilisateurs, et les temps d'oubli des utilisateurs > 10s:

```
data_clean <- data[data$erreur == 0 & data$temps <= 10000, ]
```

Pour les graphiques, je ne vais pas parler de temps de réponse mais de temps de réponse moyen.

Pour pouvoir faire cela, voici un exemple :

```
participant_means <- data_clean %>%
  group_by(numero_utilisateur) %>%
  summarise(mean_time = mean(temps, na.rm = TRUE))
```

Voici le temps de réponse moyen entre participants.

![Image d'exemple](mean_rep_between_participants.jpg)

Sur ce graphique, chaque barre représente un participant avec son temps moyen.
On constate qu'il y a une différence du temps moyen entre chaque utilisateur.
Cela dépend à la réactivité de chacun ainsi à la précision pour éviter les fautes.

Voici le temps de réponse moyen entre blocs/répétitions.

![Image d'exemple](mean_rep_between_repetitions.jpg)

Sur ce graphique, la barre de gauche représente la répétition 0 et la barre de droite la répétition 1.
On peut constater que le temps moyen est plus faible pour la répétition 1, on peut donc dire qu'il y a un effet d'apprentissage étant donné que la séquence des 15 bulles à sélectionner reste le même.

Voici le temps de réponse moyen entre techniques.

![Image d'exemple](mean_rep_between_techniques.jpg)

Sur ce graphique, chaque barre correspond aux trois techniques.
On peut constater ici que les techniques Bubble et Rope sont plus ou moins similaires en terme de temps moyen, et la technique Normal a un temps moyen bien plus elevé que les deux autres.
Cela peut s'expliquer car Bubble et Rope n'ont pas besoin d'être dans la bulle pour être sélectionné, il suffit juste que le curseur soit le plus proche de la bulle.
Tandis que la technique Normal doit forcément être sélectionné à l'intérieur de la bulle.

Voici le temps moyen de réponse entre densités.

![Image d'exemple](mean_rep_between_densities.jpg)

Sur ce graphique, chaque barre correspond aux trois densités différentes (30, 60, 90).
On peut constater ici que il n'y a pas de différence notable sur le temps moyen lorsque l'on passe de 30 à 60 de densités. Par contre, avec 90 on peut constater que le temps moyen augmente comparer aux densités de 30 et 60.
On peut expliquer cela parce que plus il y a de bulles dans l'application, plus il faut être précis.

Voici le temps moyen de réponse entre chaque tailles

![Image d'exemple](mean_rep_between_tailles.jpg)

Sur ce graphique, chaque barre correspond aux trois tailles différentes de bulles (9, 12 et 18).
On peut constater ici que plus la taille des bulles augmentent, plus le temps moyen des réponses est réduit.
On peut expliquer cela par le fait que plus la taille des bulles est grosses, moins il faut être précis pour cliquer.


Voici le temps moyen de réponse entre techniques et densités

![Image d'exemple](mean_rep_between_techniques_densities.jpg)

Sur ce graphique, on peut voir qu'on a trois groupements de barres (correspondant aux trois densités : 30, 60, 90) qui contient trois barres (correspondant aux techniques : Bubble, Normal, Rope).
On peut constater que le changement de densité affecte certaines techniques lorsqu'il y a plus de bulles/cibles.
On peut en effet constater une augmentation du temps moyen de réponse pour les techniques Bubble et Rope, alors que Normal stagne sur les trois densités.
On peut expliquer cela car Bubble et Rope sont deux techniques n'ayant pas besoin d'être dans la bulle/cible pour être sélectionner, mais il faut être le plus proche possible. Le fait d'avoir plus de bulles/cibles entraînent donc de la précision supplémentaire pour ces deux techniques.

Voici le temps moyen de réponse entre techniques et tailles de cibles.

![Image d'exemple](mean_rep_between_techniques_tailles.jpg)

Sur ce graphique, on peut voir qu'on a trois groupements de barres (correspondant aux tailles des bulles/cibles) qui contient trois barres (correspondant aux techniques : Bubble, Normal, Rope).
On peut constater que le changement de taille a un impact majeur de la diminution de temps moyen de réponse pour les trois techniques.
On peut donc en déduire que plus la cible est grande, moins l'utilisateur doit être précis, ce qui raccourcit donc le temps moyen des réponses.

Conclusion :

Pour conclure, pour avoir le temps de réponse le plus bas possible, il faudrait optimiser les facteurs importants comme la densité, la taille et la technique choisit.
Pour pouvoir être le plus efficace possible, il faudrait une taille des bulles/cibles plus grandes, une densité plutôt faible (avec une densité elevé, cela pourrait augmenter la précision et donc le temps de réponse), et plutôt choisir un style de technique comme Bubble ou Rope (de sorte à choisir la cible/bulle la plus proche et non devoir sélectionner la cible).