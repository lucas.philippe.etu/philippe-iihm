# Programmation d'une technique d'interaction : le Bubble Cursor

## Fonctionnement 

Le programme génère des cibles aléatoires en utilisant la fonction generate_targets.

Les cibles sont stockées dans un fichier CSV pour une utilisation ultérieure.

Note 1 : Les cibles sont généré une fois pour tous les fichiers csv afin de ne pas rendre une experience (groupement type de curseur, nombre et taille de cible ...) plus simple ou plus compliqué d'accès qu'une autre.

Note 2 : Les cibles seront selectionnné dans l'ordre pour la meme raison. On suppose que l'utilisateur ne pourra pas se souvenir de la séquence de cible qui apparait.

L'interface permet de définir les paramètres de l'expérience, tels que le nombre de cibles à afficher, la taille de ces cibles et la distance minimale entre elles.
Les cibles sont affichées à l'écran en utilisant la classe Target qui définit leur couleur et leur forme en utilisant PyQt5.
L'utilisateur peut cliquer sur les cibles pour les sélectionner.

Ajout :

A la fin de l'experience, les donnes enregistré s'affiche afin qu'un utilisateur distant puisse nous envoyer ces résultats. On peut donc avoir des utilisateurs d'horizons différents.

## Utilisation

Générations des fichiers de cibles :

```
python3 Generate_targets.py 
```

Exécutez le fichier MainExp.py pour lancer l'interface graphique.

```
python3 MainExp.py 
```

Définissez les paramètres de l'expérience en utilisant la section dédiée dans l'interface.

- Numéro d'utilisateur
- Technique de curseur à utilisé en premier
- Nombre de répétition de l'expérience
