import sys
from PyQt5.QtWidgets import QApplication
from src.widgets.EditeurWindow import EditeurWindow

def main():
    app = QApplication(sys.argv)
    window = EditeurWindow()
    #window.resize(575, 200)
    window.show()
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()