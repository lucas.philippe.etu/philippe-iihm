from PyQt5.QtCore import QRect, QPoint, QSize

class Key:
    def __init__(self, x, y, keyWidth, keyHeight, keySpacing, width, symbol):
        self.x = x * (keyWidth + keySpacing)
        self.y = y * (keyHeight + keySpacing)
        self.symbol = symbol
        self.rect = QRect(self.x, self.y, (width * (keyWidth + keySpacing) - keySpacing), keyHeight)

    def isOver(self, point):
        adjustedRect = self.rect.adjusted(5, 5, -5, -5)
        return adjustedRect.contains(point)

    def getCenter(self):
        return self.rect.center()
