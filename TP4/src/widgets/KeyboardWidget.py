import json

from PyQt5.QtCore import Qt, QSize, QPoint, pyqtSignal
from PyQt5.QtGui import QPainter, QColor, QPen
from PyQt5.QtWidgets import QWidget
from documents.FileDefinition import LAYOUT_FILE, MOTS_FILE
from src.model.Key import Key
from src.module.Dictionnaire import Dictionnaire
from src.module.Dwt_algo import DwtAlgo
from src.module.Reconnaisseur import resample

DEFAULT_COLOR = QColor("white")
OVER_COLOR = QColor("gray")

PEN_COLOR = QColor(0, 0, 255, 128)

PEN_COLOR2 = QColor(255, 0, 0, 255)

PEN_WIDTH = 5

WordSelectorHeight = 0
distanceStroke = 50


class KeyboardWidget(QWidget):
    newletter = pyqtSignal(str)
    def __init__(self):
        super().__init__()
        self.dwt = None
        self.letter_selected = None
        self.keys = []
        self.keyWidth = None
        self.keyHeight = None
        self.keySpacing = None

        self.keyboardWidth = 0
        self.keyboardHeight = 0
        self.trace = []
        self.resampledTrace = []
        self.nbReco = 0

        self.mousePos = QPoint(0,0)
        self.pressedKey = None

        self.wordSelectors = []
        # Initialiser le clavier
        self.loadLayoutJson()
        self.setMouseTracking(True)

        # Initialiser le dictionnaire
        self.dictionnaire = Dictionnaire()
        self.initDictionnaire()

        # Initialiser le wordSelector
        self.initWordSelector()



        self.update()

    def loadLayoutJson(self):
        # Chargement du fichier JSON contenant les paramètres du clavier
        with open(LAYOUT_FILE, "r") as f:
            layout_clavier = json.load(f)

        # Accéder aux paramètres

        self.nbReco = layout_clavier['nbReco']
        self.keyWidth = layout_clavier['keyWidth']
        self.keyHeight = layout_clavier['keyHeight']
        self.keySpacing = layout_clavier['keySpacing']

        # Créer la clé de chaque touche
        for keyData in layout_clavier['keys']:
            self.keys.append(self.createKey(keyData))

        # Créer les touches de selection de mot
        # word_selector_key_width = 10 / self.nbReco
        word_selector_key_width = 2.5
        print(word_selector_key_width)
        for i in range(self.nbReco):
            key = Key(i * word_selector_key_width, 0, self.keyWidth, self.keyHeight, self.keySpacing, word_selector_key_width, '')
            self.wordSelectors.append(key)
            self.keys.insert(0, key)


    def initWordSelector(self):
        self.dwt = DwtAlgo(self.nbReco, self.dictionnaire)

    def initDictionnaire(self):
        with open(MOTS_FILE, "r") as file:
            lines = file.readlines()

        for line in lines:
            word = line.strip()
            self.wordToStroke(word, distanceStroke)

    def createKey(self, keyData):
        key = Key(keyData['x'], keyData['y'] + WordSelectorHeight, self.keyWidth, self.keyHeight, self.keySpacing, keyData['width'], keyData['symbol'])
        bottomRightPoint = key.rect.bottomRight()
        currentHeight = bottomRightPoint.y() + self.keySpacing
        currentWidth = bottomRightPoint.x() + self.keySpacing

        if self.keyboardHeight < currentHeight:
            self.keyboardHeight = currentHeight

        if self.keyboardHeight < currentWidth:
            self.keyboardWidth = currentWidth
        return key

    def paintEvent(self, event):
        painter = QPainter(self)

        painter.setRenderHint(QPainter.Antialiasing)

        # Dessiner les touches du clavier
        for key in self.keys:
            if key.isOver(self.mousePos):
                self.paintKey(painter, key, OVER_COLOR)
            else:
                self.paintKey(painter, key, DEFAULT_COLOR)

        # Dessiner le tracé courant
        pen = QPen(PEN_COLOR, PEN_WIDTH, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)
        pen2 = QPen(PEN_COLOR2, PEN_WIDTH, Qt.SolidLine, Qt.RoundCap, Qt.RoundJoin)

        # Dessiner la ligne entre les points
        for i in range(len(self.trace) - 1):
            painter.setPen(pen)
            painter.drawLine(self.trace[i][0], self.trace[i][1], self.trace[i + 1][0], self.trace[i + 1][1])

        # Dessiner les points du tracé resample
        for i in range(len(self.resampledTrace) - 1):
            painter.setPen(pen2)
            painter.drawPoint(self.resampledTrace[i][0], self.resampledTrace[i][1])

    def sizeHint(self):
        return QSize(self.keyboardWidth, self.keyboardHeight)

    def mouseMoveEvent(self, event):
        self.mousePos = event.pos()
        self.update()

    def mousePressEvent(self, event):
        position = event.pos()
        self.trace.append((position.x(), position.y()))
        for key in self.keys:
            if key.isOver(self.mousePos):
                self.pressedKey = key
                self.letter_selected = key.symbol
                return

    def mouseReleaseEvent(self, event):
        position = event.pos()
        self.resampledTrace.append((position.x(), position.y()))
        for key in self.keys:
            if key.isOver(self.mousePos):
                if key == self.pressedKey:
                    self.newletter.emit(key.symbol)
                    if len(key.symbol) > 1:
                        for key_selector in self.wordSelectors:
                            key_selector.symbol = ''
                        self.clearTrace()
                        return
                    elif len(key.symbol) == 0:
                        self.clearTrace()
                        return
                    break
                else:
                    break

        # Nouvelle section: Imprimer les lettres sélectionnées par le tracé ré-échantillonné
        print("Lettres sélectionnées par le tracé ré-échantillonné :")
        selected_symbols = set()  # Utiliser un ensemble pour éviter les duplicatas
        for point in self.resampledTrace:
            for key in self.keys:
                if key.isOver(QPoint(*point)):
                    if key.symbol not in selected_symbols:  # Pour éviter de réimprimer les mêmes symboles
                        print(key.symbol, end=' ')
                        selected_symbols.add(key.symbol)
        print()  # Pour le retour à la ligne après l'impression des symboles

        best_words = self.dwt.compare_to_dictionary(self.resampledTrace, self.letter_selected)

        for i in range(len(best_words)):
            self.wordSelectors[i].symbol = best_words[i]

        self.clearTrace()

    def paintKey(self, painter, key, color):
        painter.setBrush(color)
        painter.drawRect(key.rect)
        painter.drawText(key.rect, Qt.AlignCenter, key.symbol)

    def clearTrace(self):
        self.trace = []
        self.resampledTrace = []
        self.update()
        self.pressedKey = None

    def mouseMoveEvent(self, event):
        self.mousePos = event.pos()

        if self.pressedKey is not None:
            point = (self.mousePos.x(), self.mousePos.y())
            self.trace.append(point)
            self.resampledTrace = resample(self.trace, distanceStroke)
        self.update()

    def wordToStroke(self, word: str, d: int):
        strokes = []
        for letter in word:
            for key in self.keys:
                if key.symbol == letter:
                    point = key.getCenter()
                    strokes.append((point.x(), point.y()))
                    break

        # Ajouter le mot au dictionnaire
        resampledTrace = resample(strokes, d)
        self.dictionnaire.addWord(word, resampledTrace, d)




