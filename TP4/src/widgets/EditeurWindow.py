from PyQt5.QtWidgets import QWidget, QLineEdit, QVBoxLayout
from src.widgets.KeyboardWidget import KeyboardWidget
# from PySide6.QtCore import Slot # This line might not be necessary unless you're using it elsewhere

class EditeurWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle('Editeur')

        # Define line_edit as an attribute of the class
        self.line_edit = QLineEdit(self)
        keyboard_widget = KeyboardWidget()

        # Création du layout et ajout des widgets
        layout = QVBoxLayout()
        layout.addWidget(self.line_edit) # Use self.line_edit here
        layout.addWidget(keyboard_widget)
        self.setLayout(layout)

        # Connexion du signal
        keyboard_widget.newletter.connect(self.addCharacter)

    def addCharacter(self, character):
        currentText = self.line_edit.text()  # This should work now
        # Add a space only if "character" is more than one letter (meaning it's a word)
        if len(character) > 1:
            newText = currentText + character + ' '
        else:
            newText = currentText + character
        self.line_edit.setText(newText)
