class Mot:
    def __init__(self, word: str, stroke: list, distance: int):
        self.word = word
        self.stroke = stroke
        self.distance = distance


class Dictionnaire:
    def __init__(self):
        self.words = dict()
        for letter in list(map(chr, range(97, 123))):
            self.words[letter] = []
    def addWord(self, word: str, stroke: list, distance: int):
        # get first letter of word
        letter = word[0]
        self.words[letter].append(Mot(word, stroke, distance))
