import math


def interpolate(a, b, d):
    ax, ay = a
    bx, by = b
    ab_dist = math.sqrt((bx - ax) ** 2 + (by - ay) ** 2)
    ratio = d / ab_dist
    return ax + ratio * (bx - ax), ay + ratio * (by - ay)


def resample(stroke, d):
    if not stroke:
        return []

    resampled = [stroke[0]]
    remaining_distance = d

    for i in range(1, len(stroke)):
        current_point = stroke[i - 1]
        next_point = stroke[i]
        distance = math.sqrt((next_point[0] - current_point[0]) ** 2 + (next_point[1] - current_point[1]) ** 2)

        if (distance >= remaining_distance):
            new_point = interpolate(current_point, next_point, remaining_distance)
            resampled.append(new_point)
            stroke.insert(i, new_point)
            remaining_distance = d
        else:
            remaining_distance -= distance

    return resampled
