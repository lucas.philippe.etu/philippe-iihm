import numpy as np


def find_distance(s, t):
    len_s, len_t = len(s), len(t)
    DTW = np.zeros((len_s + 1, len_t + 1))
    DTW[1:, 0] = np.inf
    DTW[0, 1:] = np.inf

    for i in range(1, len_s + 1):
        for j in range(1, len_t + 1):
            cost = abs(s[i - 1][1] - t[j - 1][1]) + abs(s[i - 1][0] - t[j - 1][0])
            min_val = min(DTW[i - 1, j], DTW[i, j - 1], DTW[i - 1, j - 1])
            DTW[i, j] = cost + min_val

    return DTW[len_s, len_t]


class DwtAlgo:
    def __init__(self, nb_of_words, dictionary):
        self.nb_of_words = nb_of_words
        self.dictionary = dictionary

    def compare_to_dictionary(self, s, first_letter):
        min_distances = [np.inf] * self.nb_of_words
        best_words = [None] * self.nb_of_words

        for template in self.dictionary.words[first_letter]:

            distance = find_distance(s, template.stroke)
            for i in range(3):
                if distance < min_distances[i]:
                    min_distances[i+1:] = min_distances[i:-1]
                    min_distances[i] = distance
                    best_words[i+1:] = best_words[i:-1]
                    best_words[i] = template.word
                    break

        return best_words
