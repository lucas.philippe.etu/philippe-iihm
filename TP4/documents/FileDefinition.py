import os

DATA_DIR = os.path.dirname(os.path.abspath(__file__))

LAYOUT_FILE = os.path.join(DATA_DIR, 'layout.json')
MOTS_FILE = os.path.join(DATA_DIR, 'mots.txt')
