/**
* Creates legend for altitude values using a color gradient
*/
void drawAltitudeLegend(float minVal, float maxVal, float x, float y, float w, float h) {
  int barCount = 50;
  float barWidth = w / barCount;
  float highestBarHeight = h * 0.9;
  
  // Draw title
  fill(0);
  textAlign(CENTER, CENTER);
  text("Altitude distribution", x + w / 2, y - 10);
  
  // Draw axis
  stroke(0);
  line(x, y + h, x + w, y + h); // x axis
  
  // Draw labels
  fill(0);
  textAlign(CENTER, CENTER);
  text(minVal, x, y + h + 10);
  text(maxVal, x + w, y + h + 10);
 
  // Draw bars
  for (int i = 0; i < barCount; i++) {
    float porcentageAlt = map(i, 0, barCount, 0, 100);
    float hue = getColorForAltitude(porcentageAlt);
    fill(hue, 100, 100);
    noStroke();
    rect(x + i * barWidth, y, barWidth, highestBarHeight);
  }
}

void drawDistributionPopulationLegend(int minPopulation, int maxPopulation, float x, float y, float width, float height, int numberOfDiv, float[] populationValues) {
  int[] histogram = new int[numberOfDiv];
  for (int i = 0; i < populationValues.length; i++) {
    int val = (int) map(populationValues[i], minPopulation, maxPopulation, 0, numberOfDiv);
    if (val >= 0 && val < numberOfDiv) {
      histogram[val]++;
    }
  }
  
  // Recherche du maximum de l'histogramme
  int maxHistogram = 0;
  for (int i = 0; i < histogram.length; i++) {
    if (histogram[i] > maxHistogram) {
      maxHistogram = histogram[i];
    }
  }
  
  fill(220, 100, 100);
  
  // Dessin de l'axe et des barres de l'histogramme
  float binWidth = width / numberOfDiv;

  fill(220, 100, 100);
  
  for (int i = 0; i < numberOfDiv; i++) {
    float currentX = x + i * binWidth;
    float barHeight = map(histogram[i], 0, maxHistogram, 0, height);
    rect(currentX, y - barHeight, binWidth, barHeight);
  }

  // Draw title
  fill(0);
  textAlign(CENTER, CENTER);
  text("Distribution de la Population", x + width / 2, y + height + 10);
  text(minPopulation, x, y + 10);
  text(maxPopulation, x + width, y + 10);

  for (int i = 0; i <= numberOfDiv; i++) {
    float value = map(i, 0, numberOfDiv, minPopulation, maxPopulation); // Valeur du label
    String label = nf(value, 0, 0); // Formatage du label
    float xPos = map(i, 0, numberOfDiv, 0, width); // Position x du label
    line(x + xPos , y , x + xPos, y + 4);
  }
}
