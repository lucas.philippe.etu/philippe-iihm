class City {
      int postalcode; 
      String name; 
      float x; 
      float y; 
      float population; 
      float density; 
      float altitude;
      float radius=0;
      boolean isHighlighted=false;
      boolean isClicked=false;

      public City(String postalcode, String name, float x, float y, float population, float surface, float altitude) { 
      this.postalcode = Integer.parseInt(postalcode);
      this.name = name; 
      this.x = x; 
      this.y = y; 
      this.population = population; 
      this.density = population/surface ;
      this.altitude = altitude;
    }
    
    void draw() {
    float porcentageAlt = map(this.altitude, minAltitude, maxAltitude, 0, 100);
    float hue = getColorForAltitude(porcentageAlt);
    radius=map(this.population, minPopulation, maxPopulation, 3, 100);
    float alpha = 80;
    if(isHighlighted){
      alpha=255;
    }
    fill(hue, 100, 100, alpha);
    ellipse(x, y, radius, radius);

    if (isHighlighted) {
      textSize(16);
      textAlign(LEFT, CENTER);
      float textWidth = textWidth(name);
      float rectWidth = textWidth + 10;
      float rectHeight = 20;
      fill(255);
      rect(x + radius/2 + 5, y - rectHeight/2, rectWidth, rectHeight, 5);
      
      if(isClicked){
        colorMode(RGB);
        fill(255, 0, 0);
      }
      else {
        fill(0);
      }
      text(name, x + radius/2 + 10, y - 2);
      setBasePen();
  }

}

    boolean contains(int px, int py){
      float dist = dist(x, y, px, py);
      return dist<radius/2+1;
    }


}
    
