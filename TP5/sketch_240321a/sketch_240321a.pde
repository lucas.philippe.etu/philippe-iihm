//globally
float minX, maxX;
float minY, maxY;
int totalCount;
int minPopulation, maxPopulation;
int minSurface, maxSurface;
int minAltitude, maxAltitude;
int x = 1;
int y = 2;

City cities[];

int legendsHeight=100;
int minPopulationToDisplay=1;
int maxPopulationToDisplay=300000;
boolean redrawInProgress=false;
City lastCitySelected=null;
City lastCityClicked=null;

float sliderX, sliderY, sliderWidth, sliderHeight;
float sliderPositionMin, sliderPositionMax;
boolean draggingSlider = false;
boolean draggingMin = false;

void setup() {
  size(900,900);
  colorMode(HSB, 360, 100, 100);
  readData();
  noLoop();
  
  sliderX = 50;
  sliderY = 50;
  sliderWidth = width - 100;
  sliderHeight = 20;
  // Initialise les positions des curseurs sur le slider
  sliderPositionMin = map(minPopulationToDisplay, minPopulation, maxPopulation, sliderX, sliderX + sliderWidth);
  sliderPositionMax = map(maxPopulationToDisplay, minPopulation, maxPopulation, sliderX, sliderX + sliderWidth);
}

void draw() {
  if (!redrawInProgress) {
    background(255);
    fill(0);
    textSize(20);
    textAlign(CENTER, CENTER);
    text("Afficher les populations entre " + minPopulationToDisplay + " et " + maxPopulationToDisplay, width/2, 20);
    setBasePen();
    
    drawSlider();

    for (int i = 0; i < Math.min(totalCount, cities.length); ++i) {
      cities[i].draw();
    }

    updatePixels();
    
    if (legendsHeight > 0) {
      drawLegend();
    }
  }
}

void drawSlider() {
  fill(180);
  rect(sliderX, sliderY, sliderWidth, sliderHeight);

  fill(255, 0, 0); // Poignée pour le minimum
  ellipse(sliderPositionMin, sliderY + sliderHeight / 2, 20, 20);
  fill(0, 255, 0); // Poignée pour le maximum
  ellipse(sliderPositionMax, sliderY + sliderHeight / 2, 20, 20);
}

void readData() {
  ArrayList<City> filteredCities = new ArrayList<City>();
  String[] lines = loadStrings("./villes.tsv");

  if (lines != null && lines.length > 2) {
    parseInfo(lines[0]);
    for (int i = 2; i < lines.length; i++) {
      String[] columns = split(lines[i], TAB);
      float population = float(columns[5]);
      if (population >= minPopulationToDisplay && population <= maxPopulationToDisplay) {
        filteredCities.add(new City(columns[0], columns[4], mapX(float(columns[x])), mapY(float(columns[y])), population, float(columns[6]), float(columns[7])));
      }
    }
  }

  cities = filteredCities.toArray(new City[filteredCities.size()]);
  totalCount = filteredCities.size();
  redrawInProgress = false;
}

void parseInfo(String line) {
  String infoString = line.substring(2);
  String[] infoPieces = split(infoString, ',');
  totalCount = int(infoPieces[0]);
  minX = float(infoPieces[1]);
  maxX = float(infoPieces[2]);
  minY = float(infoPieces[3]);
  maxY = float(infoPieces[4]);
  minPopulation = int(infoPieces[5]);
  maxPopulation = int(infoPieces[6]);
  minSurface = int(infoPieces[7]);
  maxSurface = int(infoPieces[8]);
  minAltitude = int(infoPieces[9]);
  maxAltitude = int(infoPieces[10]);
}

float mapX(float x) {
 return map(x, minX, maxX, 50, 800);
}

float mapY(float y) {
  return map(y, maxY, minY, 50, 800);
}

void drawLegend() {
  float distributionX = 50;
  float distributionY = height - legendsHeight;
  float distributionWidth = width - 100;
  float distributionHeight = 50;
  float[] populationValues = new float[cities.length];
  for (int i = 0; i < cities.length - 2; i++) {
    populationValues[i] = cities[i].population;
  }

  int numberOfDiv = 30;
  // draw the population distribution
  //drawAltitudeLegend(minAltitude, maxAltitude, distributionX, distributionY, distributionWidth, distributionHeight);
  drawDistributionPopulationLegend(minPopulation, maxPopulation, distributionX, distributionY, distributionWidth, distributionHeight, numberOfDiv, populationValues);
}

void keyPressed() {
  if(redrawInProgress) {
    return;
  }
  int gap=2;
  if (key == '+') {
    if (minPopulationToDisplay == 0) {
      minPopulationToDisplay = 1;
    }
    minPopulationToDisplay = minPopulationToDisplay * gap;
    readData();
  } else if (key == '-') {
    minPopulationToDisplay = minPopulationToDisplay / gap;
    readData();
  }
  redraw();
}

void mouseMoved() {
  City city = pick(mouseX, mouseY);
  if (city != null) {
    if (lastCitySelected != city) {
      if (lastCitySelected != null) {
        lastCitySelected.isHighlighted = false;
      }
      lastCitySelected = city;
      //println(city.name);
      lastCitySelected.isHighlighted = true;
      redraw();
    }
  } else {
    if (lastCitySelected != null) {
      lastCitySelected.isHighlighted = false;
      lastCitySelected = null;
      redraw();
    }
  }
}

void mousePressed(){
  float distMin = dist(mouseX, mouseY, sliderPositionMin, sliderY + sliderHeight / 2);
  float distMax = dist(mouseX, mouseY, sliderPositionMax, sliderY + sliderHeight / 2);

  if (distMin < 10) {
    draggingSlider = true;
    draggingMin = true;
  } else if (distMax < 10) {
    draggingSlider = true;
    draggingMin = false;
  }
}

void mouseDragged() {
  if (draggingSlider) {
    boolean updateData = false;
    if (draggingMin) {
      float newPos = constrain(mouseX, sliderX, sliderPositionMax);
      if (newPos != sliderPositionMin) {
        sliderPositionMin = newPos;
        minPopulationToDisplay = int(map(sliderPositionMin, sliderX, sliderX + sliderWidth, minPopulation, maxPopulation));
        updateData = true;
      }
    } else {
      float newPos = constrain(mouseX, sliderPositionMin, sliderX + sliderWidth);
      if (newPos != sliderPositionMax) {
        sliderPositionMax = newPos;
        maxPopulationToDisplay = int(map(sliderPositionMax, sliderX, sliderX + sliderWidth, minPopulation, maxPopulation));
        updateData = true;
      }
    }
    if (updateData) {
      readData();
      redraw();
    }
  }
}

void mouseReleased() {
  draggingSlider = false;
  redraw();
}



City pick(int px, int py) {
  for (int i = totalCount - 1 ; i >= 0; --i) {
    if (cities[i].contains(px, py)) {
      return cities[i];
    }
  }
  return null;
}
