float getColorForAltitude(float altitude) {
    return map(altitude, 0, 100, 230, -50);
}

void setBasePen() {
    colorMode(HSB);
    fill(0);
    textSize(12);
}
