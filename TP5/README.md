# Visualisation processing

## Première paartie
**Enoncé du tp : http://malacria.com/teachings/RVA/visualisation/tpvisu1.html**


## Deuxième partie
**Enoncé du tp : http://malacria.com/teachings/RVA/visualisation/tpvisu2.html**

### Définir les marques

Nous choisisons les attributs "population" et "altitude" pour encoder visuellement les villes. Voici comment nous pourrions associer une variable rétinienne à chaque attribut :

- Population : la variable rétinienne associée à la population sera la taille du marqueur. Ainsi, une ville avec une population plus élevée aurait une marque plus grande qu'une ville avec une population plus faible.

- Altitude : la variable rétinienne associée à l'altitude sera la couleur du marqueur. Par exemple, les villes ayant une altitude plus élevée seront représentées par des marqueurs jaunes à rouge, tandis que les villes ayant une altitude plus basse seront représentées par des marqueurs bleus.

On met en place aussi un histogramme des valeurs de population, c'est-à-dire le nombre de valeurs appartenant à chaque intervalle de l'axe afin d'améliorer la visualisation des données.
 

## Troisième partie
**Enoncé du tp : http://malacria.com/teachings/RVA/visualisation/tpvisu3.html**

Je me suis arrêté juste avant la 10eme partie.